package com.theappengers.configs

data class DatabaseConfig(
    val url: String,
    val user: String,
    val password: String
)
